#!/usr/bin/env python3
from ev3dev.ev3 import *
from time import sleep
import os

# constants
DARK_SURFACE = 20


class Interactor:
    def __init__(self, speed=100):
        os.system('setfont Lat15-Terminus24x12')

        self.speed = speed
        self.lcd = Screen()
        self.sound = Sound()

        # motors
        self.m_right = LargeMotor('outC')
        self.m_right.stop_action = 'brake'

        self.m_left = LargeMotor('outB')
        self.m_left.stop_action = 'brake'

        # sensors
        self.gy = GyroSensor()
        assert self.gy.connected, "Connect a single gyro sensor to any sensor port"

        self.cs_left = ColorSensor('in1')
        assert self.cs_left.connected, "Connect a color sensor to sensor port 1"

        self.cs_right = ColorSensor('in3')
        assert self.cs_right.connected, "Connect a color sensor to sensor port 3"

        self.us = UltrasonicSensor()
        assert self.us.connected, "Connect a single US sensor to any sensor port"

        # sensor modes
        self.gy.mode = 'GYRO-ANG'
        self.cs_left.mode = 'COL-REFLECT'
        self.cs_right.mode = 'COL-REFLECT'
        self.us.mode = 'US-DIST-CM'

    # high level
    def turn_right(self):
        self._back_degree(100)
        self._turn_right_degree(90)
        self._follow_right_line_to_border()

    def turn_left(self):
        pass  # TODO :P

    def next_field(self):
        self._follow_right_line_to_border()

    def object_at_next_field(self):
        return self.us.value() < 120

    def print_and_shout(self, text):
        self._print_centered_text(text)
        self.sound.speak(text).wait()
        self._clear_screen()

    def get_color(self):
        self._back_degree(150)

        self.cs_left.mode = 'RGB-RAW'
        red = self.cs_left.value(0)
        green = self.cs_left.value(1)
        blue = self.cs_left.value(2)
        self.cs_left.mode = 'COL-REFLECT'

        if red < 180 and green > 240 and blue < 190:
            color = 'blue'

        elif red > 240 and green < 130 and blue < 100:
            color = 'red'

        else:
            color = 'white'

        self._follow_right_line_to_border()

        return color

    # low level
    def _straight_forever(self):
        self.m_right.run_forever(speed_sp=self.speed)
        self.m_left.run_forever(speed_sp=self.speed)

    def _back_forever(self):
        self.m_right.run_forever(speed_sp=-self.speed)
        self.m_left.run_forever(speed_sp=-self.speed)

    def _back_degree(self, degree):
        self.m_right.run_to_rel_pos(position_sp=-degree, speed_sp=self.speed)
        self.m_left.run_to_rel_pos(position_sp=-degree, speed_sp=self.speed)

        self.m_right.wait_while('running')
        self.m_left.wait_while('running')

    def _straight_degree(self, degree):
        self.m_right.run_to_rel_pos(position_sp=degree, speed_sp=self.speed)
        self.m_left.run_to_rel_pos(position_sp=degree, speed_sp=self.speed)

        self.m_right.wait_while('running')
        self.m_left.wait_while('running')

    def _turn_right_forever(self):
        self.m_right.run_forever(speed_sp=-self.speed)
        self.m_left.run_forever(speed_sp=self.speed)

    def _turn_left_forever(self):
        self.m_right.run_forever(speed_sp=self.speed)
        self.m_left.run_forever(speed_sp=-self.speed)

    def _brake(self):
        self.m_right.stop(stop_action="brake")
        self.m_left.stop(stop_action="brake")

    def _turn_right_degree(self, degree):
        start = self.gy.value()

        self._turn_right_forever()

        while self.gy.value() < start + degree:  # loop until turn angle exceeds
            sleep(0.01)

        self._brake()

    def _drive_to_line(self):
        self._straight_forever()

        while self.cs_right.value() >= DARK_SURFACE or self.cs_left.value() >= DARK_SURFACE:
            sleep(0.1)

        self._brake()

    def _follow_right_line_to_border(self):
        speed_offset = int(self.speed * 0.5)
        on_line = self.cs_left.value() <= DARK_SURFACE
        is_turning_right = self.cs_right.value() >= DARK_SURFACE

        if is_turning_right:
            self.m_right.run_forever(speed_sp=self.speed)
            self.m_left.run_forever(speed_sp=self.speed + speed_offset)

        else:
            self.m_right.run_forever(speed_sp=self.speed + speed_offset)
            self.m_left.run_forever(speed_sp=self.speed)

        while True:
            if self.cs_left.value() <= DARK_SURFACE and not on_line:
                self._brake()
                break

            elif self.cs_left.value() >= DARK_SURFACE and on_line:
                on_line = False

            if self.cs_right.value() >= DARK_SURFACE and not is_turning_right:
                self.m_left.run_forever(speed_sp=self.speed + speed_offset)
                self.m_right.run_forever(speed_sp=self.speed)
                is_turning_right = True

            elif self.cs_right.value() <= DARK_SURFACE and is_turning_right:
                self.m_left.run_forever(speed_sp=self.speed)
                self.m_right.run_forever(speed_sp=self.speed + speed_offset)
                is_turning_right = False

    def _print_centered_text(self, to_print):
        self.lcd.clear()
        size = self.lcd.draw.textsize(to_print)
        self.lcd.draw.text((89 - size[0] / 2, 59), to_print)  # center text
        self.lcd.update()

    def _clear_screen(self):
        self.lcd.clear()
