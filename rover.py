#!/usr/bin/env python3
from interactor import Interactor
import random


class Rover:
    def __init__(self, initial_x=0, initial_y=0):
        """
        Rover startet, wenn nicht anders spezifiziert, links oben (0,0) und schaut nach rechts (deg = 0)
        """
        self.x = initial_x
        self.y = initial_y
        self.deg = 0
        self.marker = []  # list of tuples (x, y)
        self.gestein = []  # list of tuples (x, y, color (or None), water propotion (or None))

        self.interactor = Interactor()

    def fahre(self):
        """
        Der Rover bewegt sich ein Feld in Fahrtrichtung weiter.
        Sollte sich in Fahrtrichtung ein Objekt der Klasse Huegel befinden oder er sich an der Grenze der Welt befinden,
        dann erscheint eine entsprechende Meldung auf dem Display.
        """

        if self.interactor.object_at_next_field():
            self.nachricht('Zu steil!')
            return

        if self.deg == 0:
            self.x += 1

        elif self.deg == 90:
            self.y += 1

        if self.deg == 180:
            self.x -= 1

        elif self.deg == 270:
            self.y -= 1

        if self.y < 0:
            self.nachricht('Ich kann mich nicht bewegen!')
            self.y = 0
            return

        if self.x < 0:
            self.nachricht('Ich kann mich nicht bewegen!')
            self.x = 0
            return

        self.interactor.next_field()

    def drehe(self, richtung):
        """
        Der Rover dreht sich um 90 Grad in die Richtung, die mit richtung (links oder rechts) übergeben wurde.
        Sollte ein anderer Text (String) als "rechts" oder "links" übergeben werden, dann erscheint eine entsprechende
        Meldung auf dem Display.
        """
        if richtung == 'rechts':
            self.deg -= 90
            self.interactor.turn_right()

        else:
            self.nachricht('Befehl nicht korrekt!')

    def gestein_vorhanden(self):
        """
        Der Rover gibt durch einen Wahrheitswert (true oder false) zurück, ob sich auf seiner Position ein Objekt der
        Klasse Gestein befindet. Eine entsprechende Meldung erscheint auch auf dem Display.
        """
        is_white = self.interactor.get_color() == 'white'

        if not is_white:
            self.nachricht("Gestein gefunden!")

        return not is_white

    def huegel_vorhanden(self, richtung):
        """
        Der Rover überprüft, ob sich in richtung ("rechts", "links", oder "vorne") ein Objekt der Klasse Huegel befindet.
        Das Ergebnis wird auf dem Display angezeigt.
        Sollte ein anderer Text (String) als "rechts", "links" oder "vorne" übergeben werden, dann erscheint
        eine entsprechende Meldung auf dem Display.
        """

        if richtung not in ['rechts', 'links', 'vorne']:
            self.nachricht('Befehl nicht korrekt!')
            return False

        # rotate to richtung
        # check distance
        # rotate back
        if richtung == 'rechts':
            self.interactor.turn_right()
        elif richtung == 'links':
            self.interactor.turn_left()

        mountain_there = self.interactor.object_at_next_field()

        if mountain_there:
            self.interactor.print_and_shout('Da ist ein Hugel!')
        else:
            self.interactor.print_and_shout('Da ist kein Hugel!')

        if richtung == 'rechts':
            self.interactor.turn_left()
        elif richtung == 'links':
            self.interactor.turn_right()

        return mountain_there

    def analysiere_gestein(self):
        """
        Der Rover ermittelt den Wassergehalt des Gesteins auf seiner Position und gibt diesen auf dem Display aus.
        Sollte kein Objekt der Klasse Gestein vorhanden sein, dann erscheint eine entsprechende Meldung auf dem Display.
        """
        # obtain color, even if cached value is available ('animation' purposes)
        color = self.interactor.get_color()

        cached_value = [(x, y, color, water) for x, y, color, water in self.gestein if x == self.x and y == self.y]
        if cached_value:
            return_value = (True, cached_value[2], cached_value[3]) if cached_value[2] else (False, None, None)

        else:
            # translation
            if color == 'red':
                color = 'Rot'
            elif color == 'blue':
                color = 'Blau'
            else:
                color = None

            water = random.randint(0, 20) if color else None

            self.gestein.append((
                self.x,
                self.y,
                color,
                water
            ))

            return_value = (True, color, water) if color else (False, None, None)

        if return_value[0]:
            self.nachricht('Gestein untersucht! Wassergehalt ist %i%%. Farbe ist %s.'
                           % (return_value[2], return_value[1]))
        else:
            self.nachricht('Hier ist kein Gestein!')

        return return_value

    def setze_marke(self):
        """ Der Rover erzeugt ein Objekt der Klasse Markierungauf seiner Position. """
        self.marker.append((self.x, self.y))
        self.nachricht('Setze Markierung!')

    def marke_vorhanden(self):
        """
        Der Rover gibt durch einen Wahrheitswert (true oder false) zurück, ob sich auf seiner Position ein Objekt der
        Marke befindet. Eine entsprechende Meldung erscheint auch auf dem Display.
        """
        touching_here = any([m[0] == self.x and m[1] == self.y for m in self.marker])

        if touching_here:
            self.nachricht('Marke vorhanden!')
        else:
            self.nachricht('Marke nicht vorhanden!')

        return touching_here

    def entferne_marke(self):
        """
        Entfernt Marke auf aktueller Position.
        """
        try:
            self.marker.remove((self.x, self.y))
            self.nachricht('Marke entfernt!')
        except ValueError:
            pass

    def nachricht(self, nachricht):
        """
        Liest Nachricht vor und zeigt sie auf Display an.
        """
        self.interactor.print_and_shout(nachricht)
